#!/usr/bin/env python3
# demo of "BOTH" bi-directional edge detection  
# script by Alex Eames http://RasPi.tv  
# http://raspi.tv/?p=6791  
  
import ASUS.GPIO as GPIO  
from time import sleep     # this lets us have a time delay (see line 12)  
import dbus

sysbus = dbus.SystemBus()
systemd1 = sysbus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
manager = dbus.Interface(systemd1, 'org.freedesktop.systemd1.Manager')


GPIO.setmode(GPIO.BOARD)     # set up BCM GPIO numbering
button_gpio_num = 31 
#GPIO.setup(button_gpio_num, GPIO.IN)    # set GPIO25 as input (button)  
GPIO.setup(button_gpio_num, GPIO.IN ) # pull_up_down=GPIO.PUD_DOWN)

#cmds = [
#'sudo systemctl start sweetie_autonomous_behavior.service '
#]

runned = False

# Define a threaded callback function to run in another thread when events are detected  
def my_callback(channel):
    global runned
    if GPIO.input(button_gpio_num):     # if port 25 == 1  
        print("Rising edge detected on %d" % button_gpio_num)
        if(runned):
          job = manager.StopUnit('sweetie_autonomous_behavior.service', 'fail')
          runned = False
        else:
          job = manager.StartUnit('sweetie_autonomous_behavior.service', 'fail')
          runned = True

    else:                  # if port 25 != 1  
        print("Falling edge detected on %d " % button_gpio_num)
  
# when a changing edge is detected on port 25, regardless of whatever   
# else is happening in the program, the function my_callback will be run  
GPIO.add_event_detect(button_gpio_num, GPIO.BOTH, callback=my_callback)  
  

while(True):
    sleep(1)


#input("Press Enter when ready\n>")  
  
try:  
    sleep(30)         # wait 30 seconds  
    print ("Time's up. Finished!")
  
finally:                   # this block will run no matter how the try block exits  
    GPIO.cleanup()         # clean up after yourself  
