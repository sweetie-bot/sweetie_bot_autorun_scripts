# Simple script to start and stop robot behaviour on button press

## Asus Tinker Board

### Install dependencies

1. Install `gpio_lib_c`
```
mkdir -p ~/repos/sweetie/gpio; cd ~/repos/sweetie/gpio
git clone https://github.com/TinkerBoard/gpio_lib_c.git
cd gpio_lib_c
chmod a+x build
sudo ./build
```
2. Install `gpio_lib_python`
```
sudo apt-get install python3-dev
mkdir -p ~/repos/sweetie/gpio; cd ~/repos/sweetie/gpio
git clone https://github.com/TinkerBoard/gpio_lib_python.git
cd gpio_lib_python
sudo python3 setup.py install
```
3. Install other dependencies
```
sudo -s
apt install python3-pip pkg-config libdbus-1-dev libglib2.0-dev-bin -y
pip3 install wheel
pip3 install setuptools
pip3 install dbus-python
```

### Copy service files

```
sudo cp *.service /etc/systemd/system/
sudo cp run.py autonomous_behavior.sh /opt/ros/sweetie_bot/

```

### Enable 

`systemctl enable sweetie_button.service`

## Orange Pi

TODO
